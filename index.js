require('dotenv').config();

const bodyParser = require('body-parser'),
      express = require('express')
      app = express();

app.use(express.static(__dirname+"/public"))
app.use(bodyParser.json())


app.listen(4444)
